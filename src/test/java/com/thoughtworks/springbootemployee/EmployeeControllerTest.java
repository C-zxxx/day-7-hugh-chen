package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc client;
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    public void setUp() {
        employeeRepository.getEmployees().clear();
    }

    @Test
    void should_create_employee_when_createEmployee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Eisen", 22, "Male", 8000);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);

        //when
        //then
        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Eisen"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(22))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Male"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(8000));
        Employee saveEmployee = employeeRepository.getEmployees().get(0);
        assertEquals("Eisen", saveEmployee.getName());
        assertEquals(22, saveEmployee.getAge());
        assertEquals("Male", saveEmployee.getGender());
        assertEquals(8000, saveEmployee.getSalary());
    }

    @Test
    void should_return_employees_when_getAllEmployees_given() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Eisen", 22, "Male", 8000);
        Employee employee2 = new Employee(2L, "Aiden", 21, "Female", 8001);
        Employee employee3 = new Employee(3L, "Michael", 20, "Male", 8001);
        employeeRepository.getEmployees().add(employee1);
        employeeRepository.getEmployees().add(employee2);
        employeeRepository.getEmployees().add(employee3);

        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].name", containsInAnyOrder("Eisen", "Aiden", "Michael")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].age", containsInAnyOrder(22, 21, 20)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].gender", containsInAnyOrder("Male", "Male", "Female")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].salary", containsInAnyOrder(8000, 8001, 8001)));

    }

    @Test
    void should_return_employee_when_getAllEmployeesById_given_employee_id() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Eisen", 22, "Male", 8000);
        Employee employee2 = new Employee(2L, "Aiden", 21, "Female", 8001);
        Employee employee3 = new Employee(3L, "Michael", 20, "Male", 8001);
        employeeRepository.getEmployees().add(employee1);
        employeeRepository.getEmployees().add(employee2);
        employeeRepository.getEmployees().add(employee3);

        //when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees/2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Aiden"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value("21"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("Female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(8001));

    }




}
