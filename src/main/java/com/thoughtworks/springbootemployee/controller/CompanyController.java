package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    final CompanyRepository companyRepository;

    public CompanyController(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
        return companyRepository.createCompany(company);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<Company> getAllCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Company getCompanyById(@PathVariable(name = "id") Long id) {
        return companyRepository.getCompanyById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Company updateCompany(@PathVariable(name = "id") Long id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @GetMapping(params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public List<Company> getCompaniesByPaging(@RequestParam("page") int page, @RequestParam("size") int size) {
        return companyRepository.getCompaniesByPaging(page, size);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(HttpServletRequest request, HttpServletResponse response, @PathVariable(name = "id") Long companyId) throws IOException, ServletException {
        companyRepository.deleteCompany(companyId);
        request.getRequestDispatcher("/employees/deleteEmployeeInCompany/" + companyId).forward(request,response); // forward
    }

    @GetMapping("/{id}/employees")
    @ResponseStatus(HttpStatus.TEMPORARY_REDIRECT)
    public void getAllEmployeesInCompany(HttpServletResponse response, @PathVariable(name = "id") Long companyId) throws IOException {
        response.sendRedirect("/employees/" + companyId + "/employees"); // redirect
    }


}
