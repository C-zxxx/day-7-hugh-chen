package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    //    private final List<Employee> employees = new ArrayList<>();
    final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return this.employeeRepository.save(employee);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployees() {
        return employeeRepository.getEmployees();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee getEmployeeById(@PathVariable(name = "id") Long id) {
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(params = {"gender"})
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getEmployeesByGender(@RequestParam(name = "gender") String gender) {
        return employeeRepository.getEmployeesByGender(gender);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee updateEmployee(@PathVariable(name = "id") Long id, @RequestBody Employee employee) {
        return employeeRepository.updateEmployee(id, employee);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable(name = "id") Long id) {
        employeeRepository.deleteEmployee(id);
    }

    @DeleteMapping("/deleteEmployeeInCompany/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeInCompany(@PathVariable(name = "id") Long companyId) {
        employeeRepository.deleteEmployeeInCompany(companyId);
    }


    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesByPaging(@RequestParam("page") int page, @RequestParam("size") int size) {
        return employeeRepository.getEmployeesByPaging(page, size);
    }

    @GetMapping("{id}/employees")
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployeesInCompany(@PathVariable(name = "id") Long companyId) {
        return employeeRepository.getAllEmployeesInCompany(companyId);
    }
}
