package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private List<Company> companies = new ArrayList<>();

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }

    public Company createCompany(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    public Long generateId() {
        return getCompanies().stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Company> getAllCompanies() {
        return this.getCompanies();
    }


    public Company getCompanyById(Long id) {
        return getCompanies().stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .orElse(null);
    }

    public Company updateCompany(Long id, Company company) {
        Company editCompany = getCompanyById(id);
        if (editCompany == null){
            return null;
        }
        editCompany.setName(company.getName());
        return editCompany;
    }

    public List<Company> getCompaniesByPaging(int page, int size) {
        return getCompanies().stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public void deleteCompany(Long id) {
        getCompanies().removeIf(company -> company.getId().equals(id));
    }

}
