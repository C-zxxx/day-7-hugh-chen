package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        return getEmployees().stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return getEmployees().stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    public void deleteEmployee(Long id) {
        getEmployees().removeIf(employee -> id.equals(employee.getId()));
    }

    public void deleteEmployeeInCompany(Long companyId) {
        getEmployees().removeIf(employee -> employee.getCompanyId() != null && employee.getCompanyId().equals(companyId));
    }

    public Employee updateEmployee(Long id, Employee employee) {
        AtomicReference<Employee> return_employee = new AtomicReference<>(new Employee());
        getEmployees().forEach(employeeFound -> {
            if (id.equals(employeeFound.getId())) {
                employeeFound.setAge(employee.getAge());
                employeeFound.setSalary(employee.getSalary());
                employeeFound.setCompanyId(employee.getCompanyId());
                return_employee.set(employeeFound);
            }
        });
        return return_employee.get();
    }

    public List<Employee> getEmployeesByPaging(int page, int size) {
        int from = (page - 1) * size;
        return getEmployees().stream()
                .skip(from)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getAllEmployeesInCompany(Long companyId) {
        return getEmployees().stream()
                .filter(employee -> employee.getCompanyId() != null && employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }


}
